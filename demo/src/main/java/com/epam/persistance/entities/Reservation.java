package com.epam.persistance.entities;


import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
@RequiredArgsConstructor
@Data
@Table("reservation")
@ToString
public class Reservation {
    private static final String ROOM_NUMBER_NOT_NULL_MESSAGE = "roomNumber cannot be null";
    private static final String CHECK_IN_NOT_NULL_MESSAGE = "checkIn cannot be null";
    private static final String CHECK_OUT_NOT_NULL_MESSAGE = "checkOut cannot be null";
    private static final String PRICE_NOT_NULL_MESSAGE = "price cannot be null";
    @NotNull(message = ROOM_NUMBER_NOT_NULL_MESSAGE)
    private Long roomNumber;
    @NotNull(message = CHECK_IN_NOT_NULL_MESSAGE)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate checkIn;
    @NotNull(message = CHECK_OUT_NOT_NULL_MESSAGE)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate checkOut;
    @NotNull(message = PRICE_NOT_NULL_MESSAGE)
    private Integer price;
    @Id
    private Long id;

    public Reservation(final Long roomNumber, final LocalDate checkIn, final LocalDate checkOut, final Integer price) {
        this.roomNumber = roomNumber;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.price = price;
    }

}