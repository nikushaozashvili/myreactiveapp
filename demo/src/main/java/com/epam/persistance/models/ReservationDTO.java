package com.epam.persistance.models;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Objects;

@Getter
@Setter
@RequiredArgsConstructor
@Validated
@ToString
public class ReservationDTO {
    private static final String ROOM_NUMBER_NOT_NULL_MESSAGE = "roomNumber cannot be null";
    private static final String CHECK_IN_NOT_NULL_MESSAGE = "checkIn cannot be null";
    private static final String CHECK_OUT_NOT_NULL_MESSAGE = "checkOut cannot be null";
    private static final String PRICE_NOT_NULL_MESSAGE = "price cannot be null";
    @NotNull(message = ROOM_NUMBER_NOT_NULL_MESSAGE)
    private Long roomNumber;
    @NotNull(message = CHECK_IN_NOT_NULL_MESSAGE)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate checkIn;
    @NotNull(message = CHECK_OUT_NOT_NULL_MESSAGE)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate checkOut;
    @NotNull(message = PRICE_NOT_NULL_MESSAGE)
    private Integer price;
    private Long id;

    public ReservationDTO(final Long roomNumber, final LocalDate checkIn, final LocalDate checkOut, final Integer price) {
        this.roomNumber = roomNumber;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.price = price;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReservationDTO reservationDTO = (ReservationDTO) o;
        return roomNumber.equals(reservationDTO.roomNumber) && checkIn.equals(reservationDTO.checkIn) && checkOut.equals(reservationDTO.checkOut) && price.equals(reservationDTO.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, roomNumber, checkIn, checkOut, price);
    }
}
