package com.epam.persistance.repositories;


import com.epam.persistance.entities.Reservation;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Repository
public interface ReservationRepository extends ReactiveCrudRepository<Reservation, String> {
    @Query("SELECT * FROM reservation WHERE reservation.id=:id")
    Mono<Reservation> findById(@NotNull final Long id);

    @Query("DELETE FROM reservation WHERE reservation.id=:id")
    Mono<Void> deleteById(@NotNull final Long id);

    Mono<Reservation> save(@Valid final Reservation reservation);

    @Query("SELECT * FROM reservation")
    Flux<Reservation> findAll();
}