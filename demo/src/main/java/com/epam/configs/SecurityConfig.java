package com.epam.configs;

import com.epam.services.impl.UserDetailsServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UserDetailsRepositoryReactiveAuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableReactiveMethodSecurity;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;


@EnableWebFluxSecurity
@EnableReactiveMethodSecurity
public class SecurityConfig {
    public static final String ADMIN = "ADMIN";
    public static final String USER = "USER";
    public static final String USER_WITH_DOUBLE_PRICE = "USER_WITH_DOUBLE_PRICE";
    private static final String GET_UPDATE_DELETE_RESERVATION_BY_ID_URL = "/room/v1/reservation/{id}";
    private static final String GET_ALL_CREATE_RESERVATION_URL = "/room/v1/reservation/";

    @Bean
    public SecurityWebFilterChain securityWebFilterChain(final ServerHttpSecurity http) {
        return http
                .csrf().disable()
                .authorizeExchange()
                .pathMatchers(HttpMethod.DELETE, GET_UPDATE_DELETE_RESERVATION_BY_ID_URL).hasRole(ADMIN)
                .pathMatchers(HttpMethod.GET, GET_ALL_CREATE_RESERVATION_URL).hasRole(ADMIN)
                .pathMatchers(HttpMethod.GET, GET_UPDATE_DELETE_RESERVATION_BY_ID_URL).hasAnyRole(ADMIN, USER, USER_WITH_DOUBLE_PRICE)
                .pathMatchers(HttpMethod.POST, GET_ALL_CREATE_RESERVATION_URL).hasAnyRole(ADMIN, USER, USER_WITH_DOUBLE_PRICE)
                .pathMatchers(HttpMethod.PUT, GET_UPDATE_DELETE_RESERVATION_BY_ID_URL).hasAnyRole(ADMIN, USER, USER_WITH_DOUBLE_PRICE)
                .pathMatchers("/", "/swagger-ui/**", "/javainuse-openapi/**", "/swagger-ui.html", "/swagger-resources/**", "/webjars/**", "/v2/api-docs/**").permitAll()
                .anyExchange().authenticated()
                .and()
                .formLogin()
                .and()
                .httpBasic()
                .and()
                .build();
    }

    @Bean
    public ReactiveAuthenticationManager authenticationManager(UserDetailsServiceImpl userDetailsServiceImpl) {
        return new UserDetailsRepositoryReactiveAuthenticationManager(userDetailsServiceImpl);
    }
}
