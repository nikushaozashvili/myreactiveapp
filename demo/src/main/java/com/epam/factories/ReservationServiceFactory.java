package com.epam.factories;

import com.epam.persistance.repositories.ReservationRepository;
import com.epam.services.ReservationService;
import com.epam.services.impl.ReservationDoublePriceServiceImpl;
import com.epam.services.impl.ReservationServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.GrantedAuthority;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.epam.constants.LoggingConstants.*;

@Slf4j
public class ReservationServiceFactory {
    private static final String EXCEPTION_MESSAGE = "Could not resolve ReservationService for authorities: ";
    private final List<ReservationService> services;

    public ReservationServiceFactory(final ReservationRepository reservationRepository) {
        log.info(EXECUTING_LOG_MESSAGE, RESERVATION_SERVICE_FACTORY_CLASS, RESERVATION_SERVICE_FACTORY_CLASS, NO_ARGS);
        ModelMapper modelMapper = new ModelMapper();
        services = new ArrayList<>();
        services.add(new ReservationServiceImpl(reservationRepository, modelMapper));
        services.add(new ReservationDoublePriceServiceImpl(reservationRepository, modelMapper));
    }

    public ReservationService getService(final Collection<? extends GrantedAuthority> authorities) {
        log.info(EXECUTING_LOG_MESSAGE, RESERVATION_SERVICE_FACTORY_CLASS, GET_SERVICE_METHOD, authorities.toString());
        for (ReservationService service : this.services) {
            if (service.supports(authorities)) {
                return service;
            }
        }
        throw new IllegalArgumentException(EXCEPTION_MESSAGE + authorities);
    }

}