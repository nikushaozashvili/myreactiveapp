package com.epam.exceptions;


import com.epam.constants.LoggingConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.codec.DecodingException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.support.WebExchangeBindException;

@ControllerAdvice
@Slf4j
public class ExceptionsHandler {
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {WebExchangeBindException.class})
    public ResponseEntity<String> handleValidation(final WebExchangeBindException e) {
        log.info(LoggingConstants.EXECUTING_LOG_MESSAGE, LoggingConstants.CUSTOM_EXCEPTION_HANDLER, LoggingConstants.HANDLE_VALIDATION, e.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {DecodingException.class})
    public ResponseEntity<String> handleDecoding(final DecodingException e) {
        log.info(LoggingConstants.EXECUTING_LOG_MESSAGE, LoggingConstants.CUSTOM_EXCEPTION_HANDLER, LoggingConstants.HANDLE_DECODING, e.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {Exception.class})
    public ResponseEntity<String> handleException(final Exception e) {
        log.info(LoggingConstants.EXECUTING_LOG_MESSAGE, LoggingConstants.CUSTOM_EXCEPTION_HANDLER, LoggingConstants.HANDLE_EXCEPTION, e.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = {RuntimeException.class, IllegalArgumentException.class})
    public ResponseEntity<String> handleNotFound(final RuntimeException e) {
        log.info(LoggingConstants.EXECUTING_LOG_MESSAGE, LoggingConstants.CUSTOM_EXCEPTION_HANDLER, LoggingConstants.HANDLE_NOT_FOUND, e.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
    }
}