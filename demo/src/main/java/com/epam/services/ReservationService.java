package com.epam.services;

import com.epam.persistance.models.ReservationDTO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.Collection;

public interface ReservationService {
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    Mono<ReservationDTO> getReservation(@NotBlank final Long id);

    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    Mono<ReservationDTO> createReservation(@Valid final ReservationDTO reservationDTO);

    @Transactional
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    Mono<ReservationDTO> updateReservationPrice(@NotBlank final Long id, @Valid final ReservationDTO reservationDTO);

    @Transactional
    @PreAuthorize("hasRole('ADMIN')")
    Mono<Void> deleteReservation(@NotBlank final Long id);

    @PreAuthorize("hasRole('ADMIN')")
    Flux<ReservationDTO> getAllReservations();

    boolean supports(final Collection<? extends GrantedAuthority> authorities);
}