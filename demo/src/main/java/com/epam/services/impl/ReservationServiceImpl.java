package com.epam.services.impl;

import com.epam.constants.LoggingConstants;
import com.epam.exceptions.ReservationNotFoundException;
import com.epam.persistance.entities.Reservation;
import com.epam.persistance.models.ReservationDTO;
import com.epam.persistance.repositories.ReservationRepository;
import com.epam.services.ReservationService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collection;

import static com.epam.constants.LoggingConstants.*;

@Service
@Slf4j
public class ReservationServiceImpl implements ReservationService {
    public static final String RESERVATION_NOT_FOUND = "Reservation with this id doesn't exist";
    private final ReservationRepository reservationRepository;
    private final ModelMapper mapper;

    public ReservationServiceImpl(final ReservationRepository reservationRepository, final ModelMapper mapper) {
        log.info(EXECUTING_LOG_MESSAGE, RESERVATION_SERVICE_IMPL_CLASS, RESERVATION_SERVICE_IMPL_CLASS, NO_ARGS);
        this.reservationRepository = reservationRepository;
        this.mapper = mapper;

    }

    @Override
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    public Mono<ReservationDTO> getReservation(@NotNull final Long id) {
        log.info(EXECUTING_LOG_MESSAGE, RESERVATION_SERVICE_IMPL_CLASS, GET_RESERVATION, id);
        return reservationRepository.findById(id)
                .map(this::mapToDTO)
                .switchIfEmpty(Mono.error(new ReservationNotFoundException(RESERVATION_NOT_FOUND)));
    }

    @Override
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    public Mono<ReservationDTO> updateReservationPrice(@NotNull final Long id, @Valid final ReservationDTO reservationDTO) {
        log.info(EXECUTING_LOG_MESSAGE, RESERVATION_SERVICE_IMPL_CLASS, UPDATE_RESERVATION_PRICE, id);
        return this.reservationRepository.findById(id).map(this::mapToDTO)
                .flatMap(prevReservation -> {
                    prevReservation.setPrice(reservationDTO.getPrice());
                    Mono<Reservation> updatedReservationMono = this.reservationRepository.save(mapToEntity(prevReservation));
                    return updatedReservationMono.flatMap(e -> Mono.just(mapToDTO(e)));
                }).switchIfEmpty(Mono.error(new ReservationNotFoundException(RESERVATION_NOT_FOUND)));
    }

    @Override
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    public Mono<ReservationDTO> createReservation(@Valid final ReservationDTO reservationDTO) {
        log.info(EXECUTING_LOG_MESSAGE, RESERVATION_SERVICE_IMPL_CLASS, CREATE_RESERVATION, NO_ARGS);
        Reservation reservation = mapToEntity(reservationDTO);
        Mono<Reservation> updatedReservationMono = reservationRepository.save(reservation);
        return updatedReservationMono.map(this::mapToDTO);
    }

    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public Mono<Void> deleteReservation(@NotNull final Long id) {
        log.info(EXECUTING_LOG_MESSAGE, RESERVATION_SERVICE_IMPL_CLASS, DELETE_RESERVATION, id);
        return reservationRepository.deleteById(id);
    }

    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public Flux<ReservationDTO> getAllReservations() {
        log.info(EXECUTING_LOG_MESSAGE, RESERVATION_SERVICE_IMPL_CLASS, GET_ALL_RESERVATIONS, NO_ARGS);
        Flux<Reservation> reservations = reservationRepository.findAll();
        return reservations.map(this::mapToDTO);
    }

    @Override
    public boolean supports(final Collection<? extends GrantedAuthority> authorities) {
        return authorities.contains(new SimpleGrantedAuthority("ROLE_ADMIN"))
                || authorities.contains(new SimpleGrantedAuthority("ROLE_USER"));
    }

    private ReservationDTO mapToDTO(@Valid final Reservation reservation) {
        log.info(LoggingConstants.EXECUTING_LOG_MESSAGE, RESERVATION_SERVICE_IMPL_CLASS, LoggingConstants.MAP_TO_DTO, reservation);
        return mapper.map(reservation, ReservationDTO.class);
    }

    private Reservation mapToEntity(@Valid final ReservationDTO reservationDTO) {
        log.info(LoggingConstants.EXECUTING_LOG_MESSAGE, LoggingConstants.RESERVATION_SERVICE_IMPL_CLASS, LoggingConstants.MAP_TO_ENTITY, reservationDTO);
        return mapper.map(reservationDTO, Reservation.class);
    }
}