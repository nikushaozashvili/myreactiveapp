package com.epam.constants;

public class LoggingConstants {
    public static final String EXECUTING_LOG_MESSAGE = "In class {} method {} is executing with parameter {}";
    public static final String NO_ARGS = "NO ARGS";
    public static final String RESERVATION_RESOURCE_CLASS = "ReservationResource";
    public static final String GET_RESERVATION_BY_ID = "getReservationById";
    public static final String GET_ALL_RESERVATIONS = "getAllReservations";
    public static final String CREATE_RESERVATION = "createReservation";
    public static final String UPDATE_PRICE = "updatePrice";
    public static final String DELETE_RESERVATION = "deleteReservation";
    public static final String RESERVATION = "Reservation";
    public static final String RESERVATION_SERVICE_IMPL_CLASS = "ReservationServiceImpl";
    public static final String RESERVATION_DOUBLE_PRICE_SERVICE_IMPL_CLASS = "ReservationDoublePriceServiceImpl";
    public static final String GET_RESERVATION = "getReservation";
    public static final String UPDATE_RESERVATION_PRICE = "updateReservationPrice";
    public static final String REACTIVE_SPRING_APP = "ReactiveSpringApplication";
    public static final String MAIN = "main";
    public static final String CUSTOM_EXCEPTION_HANDLER = "ExceptionsHandler";
    public static final String HANDLE_VALIDATION = "handleValidation";
    public static final String HANDLE_EXCEPTION = "handleException";
    public static final String HANDLE_DECODING = "handleDecoding";
    public static final String HANDLE_NOT_FOUND = "handleNotFound";
    public static final String MAP_TO_DTO = "mapToDTO";
    public static final String MAP_TO_ENTITY = "mapToEntity";
    public static final String RESERVATION_SERVICE_FACTORY_CLASS = "ReservationServiceFactory";
    public static final String GET_SERVICE_METHOD = "getService";

    private LoggingConstants() {

    }
}