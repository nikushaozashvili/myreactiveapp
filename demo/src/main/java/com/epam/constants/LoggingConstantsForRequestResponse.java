package com.epam.constants;


public class LoggingConstantsForRequestResponse {
    public static final String SUCCESS_REQ = "Request Body : {}";
    public static final String FAIL_REQ = "Fail to log incoming http request, IO Exception : {}";
    public static final String SUCCESS_RESP = "Response Body :  {}";
    public static final String FAIL_RESP = "Fail to log outgoing http request, IO Exception : {}";
    public static final String URI = "URI : {}";
    public static final String PATH = "Request Path : {}";
    public static final String ID = "Request ID : {}";
    public static final String HEADERS = "Request Headers : {}";
    public static final String METHOD = "Request Method : {}";

    private LoggingConstantsForRequestResponse() {

    }
}