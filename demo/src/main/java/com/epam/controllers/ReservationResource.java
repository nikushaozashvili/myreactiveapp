package com.epam.controllers;

import com.epam.factories.ReservationServiceFactory;
import com.epam.persistance.models.ReservationDTO;
import com.epam.persistance.repositories.ReservationRepository;
import com.epam.services.ReservationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import static com.epam.constants.LoggingConstants.*;

@RestController
@RequestMapping(ReservationResource.ROOM_V_1_RESERVATION)
@Slf4j
@Tag(name = "ReservationResourceController APIs", description = "ReservationResourceController APIs for my Epam's Reactive app")
public class ReservationResource {
    public static final String ROOM_V_1_RESERVATION = "/room/v1/reservation/";
    private static final String ID_PARAM = "{id}";
    private static final String EMPTY_STR = "";
    private final ReservationServiceFactory reservationServiceFactory;
    private ReservationService reservationService;

    public ReservationResource(final ReservationRepository reservationRepository) {
        log.info(EXECUTING_LOG_MESSAGE, RESERVATION_RESOURCE_CLASS, RESERVATION_RESOURCE_CLASS, NO_ARGS);
        this.reservationServiceFactory = new ReservationServiceFactory(reservationRepository);
    }

    @Operation(description = "gets reservation by id", parameters = {
            @Parameter(name = "id", description = "parameter is unique id for getting reservation")
    })
    @GetMapping(path = ID_PARAM)
    @ResponseStatus(HttpStatus.OK)
    public Mono<ReservationDTO> getReservationById(@NotBlank @PathVariable final Long id,
                                                   final Authentication authentication) {
        log.info(EXECUTING_LOG_MESSAGE, RESERVATION_RESOURCE_CLASS, GET_RESERVATION_BY_ID, id);
        this.reservationService = this.reservationServiceFactory.getService(authentication.getAuthorities());
        return reservationService.getReservation(id);
    }

    @Operation(description = "gets all reservations", requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody)
    @GetMapping(path = EMPTY_STR)
    @ResponseStatus(HttpStatus.OK)
    public Flux<ReservationDTO> getAllReservations(final Authentication authentication) {
        log.info(EXECUTING_LOG_MESSAGE, RESERVATION_RESOURCE_CLASS, GET_ALL_RESERVATIONS, NO_ARGS);
        this.reservationService = this.reservationServiceFactory.getService(authentication.getAuthorities());
        return reservationService.getAllReservations();
    }

    @Operation(description = "creates reservation", requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody)
    @PostMapping(path = EMPTY_STR)
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<ReservationDTO> createReservation(@Valid @RequestBody final Mono<ReservationDTO> reservationMono,
                                                  final Authentication authentication) {
        log.info(EXECUTING_LOG_MESSAGE, RESERVATION_RESOURCE_CLASS, CREATE_RESERVATION, RESERVATION);
        this.reservationService = this.reservationServiceFactory.getService(authentication.getAuthorities());
        return reservationMono.flatMap(this.reservationService::createReservation);
    }

    @Operation(description = "updates price", requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody())
    @PutMapping(path = ID_PARAM)
    @ResponseStatus(HttpStatus.OK)
    public Mono<ReservationDTO> updatePrice(@NotBlank @PathVariable final Long id, @Valid @RequestBody final Mono<ReservationDTO> reservationMono,
                                            final Authentication authentication) {
        log.info(EXECUTING_LOG_MESSAGE, RESERVATION_RESOURCE_CLASS, UPDATE_PRICE, id);
        this.reservationService = this.reservationServiceFactory.getService(authentication.getAuthorities());
        return reservationMono.flatMap(reservation -> reservationService.updateReservationPrice(id, reservation));
    }

    @Operation(description = "deletes reservation")
    @DeleteMapping(path = ID_PARAM)
    @ResponseStatus(HttpStatus.OK)
    public Mono<Void> deleteReservation(@NotBlank @PathVariable final Long id, final Authentication authentication) {
        log.info(EXECUTING_LOG_MESSAGE, RESERVATION_RESOURCE_CLASS, DELETE_RESERVATION, id);
        this.reservationService = this.reservationServiceFactory.getService(authentication.getAuthorities());
        return reservationService.deleteReservation(id);
    }
}
