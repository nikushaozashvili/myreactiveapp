package com.epam.controllersTests;


import com.epam.persistance.entities.Reservation;
import org.assertj.core.api.Assertions;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Objects;

import static com.epam.configs.SecurityConfig.*;
import static com.epam.controllers.ReservationResource.ROOM_V_1_RESERVATION;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.springSecurity;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ReservationResourceTest {
    private static final String NON_EXISTING_ID = "4382423423427";
    private static final String ID_STR = "id";
    @Autowired
    private ApplicationContext applicationContext;
    private WebTestClient webTestClient;
    private Reservation reservation;

    @Before
    public void setUp() {
        webTestClient = WebTestClient.bindToApplicationContext(applicationContext)
                .apply(springSecurity())
                .build();
        reservation = new Reservation(100L,
                LocalDate.now(),
                LocalDate.now().plus(10, ChronoUnit.DAYS),
                200);
    }

    @Test
    public void createReservationWhenNotAuthorized() {
        webTestClient
                .post()
                .uri(ROOM_V_1_RESERVATION)
                .body(Mono.just(reservation), Reservation.class)
                .exchange()
                .expectStatus().isUnauthorized()
                .expectBody().isEmpty();
    }

    @Test
    @WithMockUser(roles = {USER, USER_WITH_DOUBLE_PRICE})
    public void createReservationWhenUser() {
        webTestClient
                .post()
                .uri(ROOM_V_1_RESERVATION)
                .body(Mono.just(reservation), Reservation.class)
                .exchange()
                .expectStatus().isCreated()
                .expectBody(Reservation.class);
    }

    @Test
    @WithMockUser(roles = ADMIN)
    public void createReservationWhenAdmin() {
        webTestClient
                .post()
                .uri(ROOM_V_1_RESERVATION)
                .body(Mono.just(reservation), Reservation.class)
                .exchange()
                .expectStatus().isCreated()
                .expectBody(Reservation.class);
    }

    @Test
    public void getReservationByNonExistingIdWhenNotAuthorized() {
        webTestClient
                .get()
                .uri(ROOM_V_1_RESERVATION + NON_EXISTING_ID)
                .exchange()
                .expectStatus().isUnauthorized()
                .expectBody().isEmpty();
    }

    @Test
    @WithMockUser(roles = USER)
    public void getReservationByNonExistingIdWhenUser() {
        webTestClient
                .get()
                .uri(ROOM_V_1_RESERVATION + NON_EXISTING_ID)
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    @WithMockUser(roles = ADMIN)
    public void getReservationByNonExistingIdWhenAdmin() {
        webTestClient
                .get()
                .uri(ROOM_V_1_RESERVATION + NON_EXISTING_ID)
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    @WithMockUser(roles = {USER, USER_WITH_DOUBLE_PRICE})
    public void getReservationByExistingIdWhenUser() throws JSONException {
        byte[] content = webTestClient
                .post()
                .uri(ROOM_V_1_RESERVATION)
                .body(Mono.just(reservation), Reservation.class)
                .exchange()
                .returnResult(Reservation.class)
                .getResponseBodyContent();
        String responseStr = new String(Objects.requireNonNull(content), StandardCharsets.UTF_8);
        JSONObject jsonObject = new JSONObject(responseStr);
        String id = jsonObject.getString(ID_STR);
        webTestClient
                .get()
                .uri(ROOM_V_1_RESERVATION + id)
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Reservation.class);
    }

    @Test
    @WithMockUser(roles = ADMIN)
    public void getReservationByExistingIdWhenAdmin() throws JSONException {
        byte[] content = webTestClient
                .post()
                .uri(ROOM_V_1_RESERVATION)
                .body(Mono.just(reservation), Reservation.class)
                .exchange()
                .returnResult(Reservation.class)
                .getResponseBodyContent();
        String responseStr = new String(Objects.requireNonNull(content), StandardCharsets.UTF_8);
        JSONObject jsonObject = new JSONObject(responseStr);
        String id = jsonObject.getString(ID_STR);
        webTestClient
                .get()
                .uri(ROOM_V_1_RESERVATION + id)
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Reservation.class);
    }

    @Test
    public void getAllReservationsWhenNotAuthorized() {
        webTestClient
                .get()
                .uri(ROOM_V_1_RESERVATION)
                .exchange()
                .expectStatus().isUnauthorized()
                .expectBody().isEmpty();
    }

    @Test
    @WithMockUser(roles = USER)
    public void getAllReservationsUser() {
        webTestClient
                .get()
                .uri(ROOM_V_1_RESERVATION)
                .exchange()
                .expectStatus().isForbidden();
    }

    @Test
    @WithMockUser(roles = ADMIN)
    public void getAllReservationsAdmin() {
        webTestClient
                .get()
                .uri(ROOM_V_1_RESERVATION)
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Reservation.class);
    }

    @Test
    public void deleteReservationByExistingIdWhenNotAuthorized() {
        webTestClient
                .delete()
                .uri(ROOM_V_1_RESERVATION + 943943)
                .exchange()
                .expectStatus().isUnauthorized()
                .expectBody().isEmpty();
    }

    @Test
    @WithMockUser(roles = {USER, USER_WITH_DOUBLE_PRICE})
    public void deleteReservationByExistingIdWhenUser() throws JSONException {
        byte[] content = webTestClient
                .post()
                .uri(ROOM_V_1_RESERVATION)
                .body(Mono.just(reservation), Reservation.class)
                .exchange()
                .returnResult(Reservation.class)
                .getResponseBodyContent();
        String responseStr = new String(Objects.requireNonNull(content), StandardCharsets.UTF_8);
        JSONObject jsonObject = new JSONObject(responseStr);
        String id = jsonObject.getString(ID_STR);
        webTestClient
                .delete()
                .uri(ROOM_V_1_RESERVATION + id)
                .exchange()
                .expectStatus().isForbidden();
    }

    @Test
    @WithMockUser(roles = ADMIN)
    public void deleteReservationByExistingIdWhenAdmin() throws JSONException {
        byte[] content = webTestClient
                .post()
                .uri(ROOM_V_1_RESERVATION)
                .body(Mono.just(reservation), Reservation.class)
                .exchange()
                .returnResult(Reservation.class)
                .getResponseBodyContent();
        String responseStr = new String(Objects.requireNonNull(content), StandardCharsets.UTF_8);
        JSONObject jsonObject = new JSONObject(responseStr);
        String id = jsonObject.getString(ID_STR);
        webTestClient
                .delete()
                .uri(ROOM_V_1_RESERVATION + id)
                .exchange()
                .expectStatus().isOk()
                .expectBody(Boolean.class);
    }

    @Test
    public void updateReservationPriceByNonExistingIdWhenNotAuthorized() {
        webTestClient
                .put()
                .uri(ROOM_V_1_RESERVATION + NON_EXISTING_ID)
                .body(Mono.just(reservation), Reservation.class)
                .exchange()
                .expectStatus().isUnauthorized()
                .expectBody().isEmpty();
    }

    @Test
    @WithMockUser(roles = USER)
    public void updateReservationPriceByNonExistingIdWhenUser() {
        webTestClient
                .put()
                .uri(ROOM_V_1_RESERVATION + NON_EXISTING_ID)
                .body(Mono.just(reservation), Reservation.class)
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    @WithMockUser(roles = ADMIN)
    public void updateReservationPriceByNonExistingIdWhenAdmin() {
        webTestClient
                .put()
                .uri(ROOM_V_1_RESERVATION + NON_EXISTING_ID)
                .body(Mono.just(reservation), Reservation.class)
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    @WithMockUser(roles = {USER, USER_WITH_DOUBLE_PRICE})
    public void updateReservationPriceByExistingIdWhenUser() throws JSONException {
        byte[] content = webTestClient
                .post()
                .uri(ROOM_V_1_RESERVATION)
                .body(Mono.just(reservation), Reservation.class)
                .exchange()
                .returnResult(Reservation.class)
                .getResponseBodyContent();
        String responseStr = new String(Objects.requireNonNull(content), StandardCharsets.UTF_8);
        JSONObject jsonObject = new JSONObject(responseStr);
        String id = jsonObject.getString(ID_STR);
        int newPrice = 9999;
        Reservation reservationWithNewPrice = new Reservation(100L,
                LocalDate.now(),
                LocalDate.now().plus(10, ChronoUnit.DAYS),
                newPrice);
        Mono<Reservation> reservationMonoWithNewPrice = Mono.just(reservationWithNewPrice);
        byte[] newContent = webTestClient
                .put()
                .uri(ROOM_V_1_RESERVATION + id)
                .body(reservationMonoWithNewPrice, Reservation.class)
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Reservation.class)
                .returnResult()
                .getResponseBodyContent();
        String newResponseStr = new String(Objects.requireNonNull(newContent), StandardCharsets.UTF_8);
        JSONObject newJsonObject = new JSONObject(newResponseStr);
        int actualNewPrice = newJsonObject.getInt("price");
        Assertions.assertThat(actualNewPrice).isIn(newPrice, 2 * newPrice);
    }

    @Test
    @WithMockUser(roles = ADMIN)
    public void updateReservationPriceByExistingIdWhenAdmin() throws JSONException {
        byte[] content = webTestClient
                .post()
                .uri(ROOM_V_1_RESERVATION)
                .body(Mono.just(reservation), Reservation.class)
                .exchange()
                .returnResult(Reservation.class)
                .getResponseBodyContent();
        String responseStr = new String(Objects.requireNonNull(content), StandardCharsets.UTF_8);
        JSONObject jsonObject = new JSONObject(responseStr);
        String id = jsonObject.getString(ID_STR);
        int newPrice = 9999;
        Reservation reservationWithNewPrice = new Reservation(100L,
                LocalDate.now(),
                LocalDate.now().plus(10, ChronoUnit.DAYS),
                newPrice);
        Mono<Reservation> reservationMonoWithNewPrice = Mono.just(reservationWithNewPrice);
        byte[] newContent = webTestClient
                .put()
                .uri(ROOM_V_1_RESERVATION + id)
                .body(reservationMonoWithNewPrice, Reservation.class)
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Reservation.class)
                .returnResult()
                .getResponseBodyContent();
        String newResponseStr = new String(Objects.requireNonNull(newContent), StandardCharsets.UTF_8);
        JSONObject newJsonObject = new JSONObject(newResponseStr);
        int actualNewPrice = newJsonObject.getInt("price");
        Assertions.assertThat(actualNewPrice).isIn(newPrice, 2 * newPrice);
    }
}
