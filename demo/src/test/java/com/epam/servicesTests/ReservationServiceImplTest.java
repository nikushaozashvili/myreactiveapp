package com.epam.servicesTests;

import com.epam.exceptions.ReservationNotFoundException;
import com.epam.persistance.models.ReservationDTO;
import com.epam.persistance.repositories.ReservationRepository;
import com.epam.services.impl.ReservationServiceImpl;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.LocalDate;
import java.util.Objects;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ReservationServiceImplTest {
    private static final long ROOM_NUMBER_1 = 10001;
    private static final LocalDate CHECK_IN_1 = LocalDate.of(2022, 1, 12);
    private static final LocalDate CHECK_OUT_1 = LocalDate.of(2022, 1, 13);
    private static final int PRICE_1 = 1000;
    private static final long ROOM_NUMBER_2 = 20002;
    private static final LocalDate CHECK_IN_2 = LocalDate.of(2023, 2, 25);
    private static final LocalDate CHECK_OUT_2 = LocalDate.of(2023, 2, 27);
    private static final int PRICE_2 = 2000;
    private static final Long NOT_EXISTING_ID = 9328312L;
    @Autowired
    private ReservationRepository reservationRepository;
    @InjectMocks
    private ReservationServiceImpl service;

    @Before
    public void setup() {
        this.service = new ReservationServiceImpl(this.reservationRepository, new ModelMapper());
    }

    @Test
    public void testCreate1() {
        ReservationDTO expectedReservation1 = new ReservationDTO(
                ROOM_NUMBER_1,
                CHECK_IN_1,
                CHECK_OUT_1,
                PRICE_1);
        Mono<ReservationDTO> expectedMono1 = this.service.createReservation(expectedReservation1);
        StepVerifier.create(expectedMono1)
                .expectNext(expectedReservation1)
                .verifyComplete();
    }

    @Test
    public void testCreate2() {
        ReservationDTO expectedReservation2 = new ReservationDTO(
                ROOM_NUMBER_2,
                CHECK_IN_2,
                CHECK_OUT_2,
                PRICE_2);
        Mono<ReservationDTO> expectedMono2 = this.service.createReservation(expectedReservation2);
        StepVerifier.create(expectedMono2)
                .expectNext(expectedReservation2)
                .verifyComplete();
    }

    @Test
    public void testCreate1And2() {
        ReservationDTO expectedReservation1 = new ReservationDTO(
                ROOM_NUMBER_1,
                CHECK_IN_1,
                CHECK_OUT_1,
                PRICE_1);
        Mono<ReservationDTO> expectedMono1 = this.service.createReservation(expectedReservation1);
        StepVerifier.create(expectedMono1)
                .expectNext(expectedReservation1)
                .verifyComplete();
        ReservationDTO expectedReservation2 = new ReservationDTO(
                ROOM_NUMBER_2,
                CHECK_IN_2,
                CHECK_OUT_2,
                PRICE_2);
        Mono<ReservationDTO> expectedMono2 = this.service.createReservation(expectedReservation2);
        StepVerifier.create(expectedMono2)
                .expectNext(expectedReservation2)
                .verifyComplete();
    }

    @Test
    public void testGetReservationIdNotExists() {
        StepVerifier.create(this.service.getReservation(NOT_EXISTING_ID))
                .expectErrorMatches(throwable -> throwable instanceof ReservationNotFoundException &&
                        throwable.getMessage().equals(ReservationServiceImpl.RESERVATION_NOT_FOUND)
                ).verify();
    }

    @Test
    public void testDelete() {
        ReservationDTO expectedReservation1 = new ReservationDTO(
                ROOM_NUMBER_1,
                CHECK_IN_1,
                CHECK_OUT_1,
                PRICE_1);
        Mono<ReservationDTO> expectedMono1 = this.service.createReservation(expectedReservation1);
        Long id = Objects.requireNonNull(expectedMono1.block()).getId();
        StepVerifier.create(this.service.deleteReservation(id)).verifyComplete();

    }

    @Test
    public void testUpdatePrice() {
        int startingPrice = 5000;
        ReservationDTO startingReservation = new ReservationDTO(
                ROOM_NUMBER_1,
                CHECK_IN_1,
                CHECK_OUT_1,
                startingPrice);
        Mono<ReservationDTO> startingReservationMono = this.service.createReservation(startingReservation);
        ReservationDTO actualReservation = startingReservationMono.block();
        Long startingReservationId = Objects.requireNonNull(actualReservation).getId();
        Integer actualPrice = actualReservation.getPrice();
        Assertions.assertThat(actualPrice).isEqualTo(startingPrice);
        int newPrice = 7000;
        ReservationDTO newReservation = new ReservationDTO(
                ROOM_NUMBER_1,
                CHECK_IN_1,
                CHECK_OUT_1,
                newPrice);
        Mono<ReservationDTO> newReservationActualMono = this.service.updateReservationPrice(startingReservationId, newReservation);
        ReservationDTO newReservationActual = newReservationActualMono.block();
        int newPriceActual = Objects.requireNonNull(newReservationActual).getPrice();
        Assertions.assertThat(newPriceActual).isEqualTo(newPrice);
    }

    @Test
    public void testUpdatePriceNonExistingId() {
        int newPrice = 7000;
        ReservationDTO newReservation = new ReservationDTO(
                ROOM_NUMBER_1,
                CHECK_IN_1,
                CHECK_OUT_1,
                newPrice);
        Mono<ReservationDTO> newReservationActualMono = this.service.updateReservationPrice(NOT_EXISTING_ID, newReservation);
        StepVerifier.create(newReservationActualMono)
                .expectErrorMatches(throwable -> throwable instanceof ReservationNotFoundException &&
                        throwable.getMessage().equals(ReservationServiceImpl.RESERVATION_NOT_FOUND)
                ).verify();

    }
}
